# Dialog Manager
Android library to manage dialogs. 

Current version: `0.3`

## Usage

### Setup activity
Your Activity should implement **DialogFragmentProvider** interface

You also need to register, and unregister the activity in the DialogManager so that he can show dialogs in activity context. Good place to do it are onResume() and onPause() 
```kotlin
override fun onResume() {
    super.onResume()
	DialogManager.registerActivity(this)
}
override fun onPause() {
    DialogManager.unregisterActivity(this)
    super.onPause()
}
```

### Display options

You can show dialogs with 4 diffrent options:
- DISMISS_PREVIOUS - removes all previously  shown dialogs and clears a queue
 - WAIT_ON_QUEUE - add new dialog to the end of a queue. When all visible dialogs will be closed, dialogs from queue will be shown
 - SHOW_IF_NO_OTHERS - Shows a new dialog only when there is no other one shown
 - SHOW_ON_TOP - Show new dialog on top of current 

### Create dialog
This library work only with DialogFragments, not regular Dialogs. In addition, they must inherit from **BaseDialogFragment**

In BaseDialogFragment you have access to **dialogProvider**, and his method **handleDialogEvent**. So you can easily trigger events from your fragment:
```kotlin
button.setOnClickListener { 
	dialogProvider?.handleDialogEvent(DialogEvent.Positive(requestId))
}
```
You can want to pass custom data tou can use:
```kotlin
button.setOnClickListener { 
	dialogProvider?.handleDialogEvent(DialogEvent.Custom(requestId, customObject))
}
```
Events: `DialogEvent.Destroy` and `DialogEvent.Cancel` are triggered automatically

You should override `requestId` value. 
Example with requestId taken from arguments:
```kotlin
override val requestId: Int by lazy { arguments.getInt(PARAM_REQUEST_ID) }
```

### Show dialogs

Showing dialog with default WAIT_ON_QUEUE option:
```kotlin
showDialog(dialogFragment)
```
You can add option as second parameter
```kotlin
showDialog(dialogFragment, DialogManager.ShowOption.DISMISS_PREVIOUS)
```
You can make sure that the new dialog will be shown only in the current activity, by adding its instance to third parameter
```kotlin
showDialog(dialogFragment, DialogManager.ShowOption.DISMISS_PREVIOUS, this)
```

### Alert dialog
You can also create AlertDialog wrapped in DialogFragment easy way:
```kotlin
alertDialog() {
	title = "Title"
	message = "message"
}
```
You can also set more parameters like buttons, list of items, requestId:

```kotlin
alertDialog(requestId) {
	title = "Title"
	message = "message"
	positiveButton = "Positive"
    negativeButton = "Negative"
    neutralString = "Neutral"
    items = arrayOf("First Item" , "Second Item")
}
```

### Handle events
To handle events from dialog, you need to override **handleDialogEvent**
Example:
```kotlin
override fun handleDialogEvent(dialogEvent: DialogEvent) {
    when (dialogEvent) {
        is DialogEvent.Positive -> {
            Log.d(TAG, "Positive action. Request id = ${dialogEvent.requestId}")
        }
        is DialogEvent.Negative -> {
            Log.d(TAG, "Negative action. Request id = ${dialogEvent.requestId}")
        }
        is DialogEvent.Neutral -> {
            Log.d(TAG, "Neutral action. Request id = ${dialogEvent.requestId}")
        }
        is DialogEvent.Cancel -> {
            Log.d(TAG, "Cancel action. Request id = ${dialogEvent.requestId}")
        }
        is DialogEvent.Destroy -> {
            Log.d(TAG, "Destroy action. Request id = ${dialogEvent.requestId}")
        }
        is DialogEvent.SelectItem -> {
            Log.d(TAG, "SelectItem action. Request id = ${dialogEvent.requestId}. Selected item id = ${dialogEvent.itemId}")
        }
        is DialogEvent.Custom -> {
            Log.d(TAG, "Custom action. Request id = ${dialogEvent.requestId}. Custom object = ${dialogEvent.customObject}")
        }
    }
}
```

## Used dependecies

	"org.jetbrains.kotlin:kotlin-stdlib-jre7:$kotlin_version"
    "com.android.support:appcompat-v7:$support_version"
    "io.reactivex.rxjava2:rxjava:$rx_version"
    "io.reactivex.rxjava2:rxandroid:$rxandroid_version"

#### Dependecies verions 

```groovy
	kotlin_version = '1.2.41'
    support_version = '27.1.1'
    rx_version = '2.1.8'
    rxandroid_version = '2.0.1'
```

## License
	Copyright 2018 BM Solution & Soft-AB

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.

