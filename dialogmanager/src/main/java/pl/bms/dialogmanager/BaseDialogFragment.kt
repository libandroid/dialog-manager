package pl.bms.dialogmanager

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.view.View
import java.util.*

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 19.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */

abstract class BaseDialogFragment: DialogFragment() {

    protected var dialogProvider: DialogFragmentProvider? = null

    protected abstract val requestId: Int

    override fun onCancel(dialog: DialogInterface) {
        dialogProvider?.handleDialogEvent(DialogEvent.Cancel(requestId))
        super.onCancel(dialog)
    }

    override fun onAttach(context: Context?) {
        try {
            dialogProvider = context as DialogFragmentProvider
        } catch (e: ClassCastException) {
            throw ClassCastException(context.toString() + " must implement DialogFragmentProvider")
        }
        super.onAttach(context)
    }

    override fun onAttachFragment(childFragment: Fragment?) {
        try {
            dialogProvider = childFragment as DialogFragmentProvider
        } catch (e: ClassCastException) {
            throw ClassCastException(childFragment.toString() + " must implement DialogFragmentProvider")
        }
        super.onAttachFragment(childFragment)
    }

    override fun onDetach() {
        dialogProvider = null
        super.onDetach()
    }

    private val randomTag: String by lazy { "tag${Random().nextLong()}" }

    override fun onDestroy() {
        if (isRemoving) {
            dialogProvider?.handleDialogEvent(DialogEvent.Destroy(requestId))
            DialogBus.emitDestroy(tag ?: randomTag)
        }
        super.onDestroy()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        DialogBus.emitCreated(tag ?: randomTag)
        super.onViewCreated(view, savedInstanceState)
    }

}