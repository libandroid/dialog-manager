@file:Suppress("unused")

package pl.bms.dialogmanager

import android.content.DialogInterface

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 19.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */

sealed class DialogEvent(val requestId: Int) {

    class Positive(requestId: Int) : DialogEvent(requestId)

    class Negative(requestId: Int) : DialogEvent(requestId)

    class Neutral(requestId: Int) : DialogEvent(requestId)

    class Cancel(requestId: Int) : DialogEvent(requestId)

    class Destroy(requestId: Int) : DialogEvent(requestId)

    class SelectItem(requestId: Int, val itemId: Int) : DialogEvent(requestId)

    class Custom(requestId: Int, val customObject: Any) : DialogEvent(requestId)

}