package pl.bms.dialogmanager

import android.support.v4.app.DialogFragment
import android.support.v7.app.AppCompatActivity

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 18.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */

interface DialogFragmentProvider {

    fun handleDialogEvent(dialogEvent: DialogEvent) {}

}

fun showDialog(dialogFragment: DialogFragment,
               option: DialogManager.ShowOption = DialogManager.ShowOption.WAIT_ON_QUEUE,
               showOnlyInActivity: AppCompatActivity? = null) {
    DialogManager.showDialog(dialogFragment, option, showOnlyInActivity)
}

inline fun alertDialog(requestId: Int = 0, block: AlertDialogFragmentBuilder.() -> Unit): AlertDialogFragment {
    return AlertDialogFragmentBuilder(requestId).also(block).create()
}