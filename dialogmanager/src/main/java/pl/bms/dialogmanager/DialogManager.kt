package pl.bms.dialogmanager

import android.support.v4.app.DialogFragment
import android.support.v7.app.AppCompatActivity
import android.util.Log
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.lang.ref.WeakReference
import java.util.concurrent.TimeUnit

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 18.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */

object DialogManager {

    private var currentActivity: WeakReference<AppCompatActivity>? = null
    private var currentDialogTags: MutableList<String> = mutableListOf()
    private val queue = mutableSetOf<DialogToShow>()
    private const val DIALOGS_TAG_PREFIX = "dialog.tag.number."

    private var fragmentCounter = 0L

    private val showSubject: PublishSubject<DialogToShow> = PublishSubject.create()

    init {
        DialogBus.observeDestroyed()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { destroyedFragmentTag ->
                    currentDialogTags.remove(destroyedFragmentTag)
                    if (currentDialogTags.isEmpty()) {
                        queue.firstOrNull()?.also {
                            showSubject.onNext(it)
                            queue.remove(it)
                        }
                    }
                }


        showSubject.subscribeOn(Schedulers.single())
                .filter { dialogToShow -> !(dialogToShow.showOnlyInActivity != null && dialogToShow.showOnlyInActivity != currentActivity?.get()) }
                .concatMap { dialogToShow ->

                    when (dialogToShow.showOption) {
                        ShowOption.DISMISS_PREVIOUS -> {
                            currentDialogTags.clear()
                            queue.clear()
                        }
                        ShowOption.WAIT_ON_QUEUE -> {
                            if (currentDialogTags.isNotEmpty()) {
                                queue.add(dialogToShow)
                                return@concatMap Observable.just(Unit)
                            }
                        }
                        ShowOption.SHOW_IF_NO_OTHERS -> {
                            if (currentDialogTags.isNotEmpty() || queue.size > 0) {
                                return@concatMap Observable.just(Unit)
                            }
                        }
                        ShowOption.SHOW_ON_TOP -> {
                            //Do nothing, dialog will be added on top in the next step
                        }
                    }

                    currentActivity?.get()?.also { activity ->
                        fragmentCounter++
                        val tag = "$DIALOGS_TAG_PREFIX$fragmentCounter"
                        currentDialogTags.add(tag)
                        dialogToShow.dialog.show(activity.supportFragmentManager, tag)
                        return@concatMap DialogBus.observeCreated()
                                .subscribeOn(Schedulers.single())
                                .filter { it == tag }
                                .timeout(1000, TimeUnit.MILLISECONDS)
                                .map {}
                                .onErrorReturn {}
                                .doOnComplete {
                                    if (dialogToShow.showOption == ShowOption.DISMISS_PREVIOUS) {
                                        dismissOthersThan(tag)
                                    }
                                }
                    }
                    Observable.just(Unit)
                }.subscribe()
    }

    fun showDialog(dialog: DialogFragment, option: ShowOption = ShowOption.WAIT_ON_QUEUE, showOnlyInActivity: AppCompatActivity? = null) {
        Log.d(this::class.java.simpleName, "showDialog with option $option")
        showSubject.onNext(DialogToShow(dialog, option, showOnlyInActivity))
    }

    private fun dismissOthersThan(tag: String) {
        currentActivity?.get()?.supportFragmentManager?.fragments?.forEach {
            if (it.tag?.contains(DIALOGS_TAG_PREFIX) == true && it is DialogFragment && it.tag != tag) {
                it.dismiss()
            }
        }
    }

    fun registerActivity(activity: AppCompatActivity) {
        currentActivity = WeakReference(activity)
    }

    fun unregisterActivity(activity: AppCompatActivity) {
        if (currentActivity?.get() === activity) {
            currentActivity = null
        }
    }

    enum class ShowOption {
        DISMISS_PREVIOUS,
        WAIT_ON_QUEUE,
        SHOW_IF_NO_OTHERS,
        SHOW_ON_TOP
    }

    private data class DialogToShow(val dialog: DialogFragment, val showOption: ShowOption, val showOnlyInActivity: AppCompatActivity?)

}