package pl.bms.dialogmanager

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 19.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */

internal object DialogBus {

    private val created: PublishSubject<String> = PublishSubject.create()
    private val destroyed: PublishSubject<String> = PublishSubject.create()

    fun emitDestroy(tag: String) {
        destroyed.onNext(tag)
    }

    fun emitCreated(tag: String) {
        created.onNext(tag)
    }

    fun observeDestroyed() : Observable<String> = destroyed

    fun observeCreated() : Observable<String> = created
}