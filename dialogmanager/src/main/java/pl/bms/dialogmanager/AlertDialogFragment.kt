package pl.bms.dialogmanager

import android.app.Dialog
import android.os.Bundle
import android.support.v7.app.AlertDialog


/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 18.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */


/**
 * AlertDialog wrapped in DialogFragment
 */
class AlertDialogFragment : BaseDialogFragment() {

    override val requestId: Int by lazy { arguments?.getInt(PARAM_REQUEST_ID) ?: 0 }
    private val title: String? by lazy { arguments?.getString(PARAM_TITLE) }
    private val message: String? by lazy { arguments?.getString(PARAM_MESSAGE) }
    private val yesString: String? by lazy { arguments?.getString(PARAM_YES_STRING) }
    private val noString: String? by lazy { arguments?.getString(PARAM_NO_STRING) }
    private val neutralString: String? by lazy { arguments?.getString(PARAM_NEUTRAL_STRING) }
    private val items: Array<String>? by lazy { arguments?.getStringArray(PARAM_ITEMS) }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(requireContext()).apply {
            setTitle(title)
            setMessage(message)
            yesString?.also {
                setPositiveButton(yesString) { _, _ ->
                    dialogProvider?.handleDialogEvent(DialogEvent.Positive(requestId))
                }
            }
            noString?.also {
                setNegativeButton(noString) { _, _ ->
                    dialogProvider?.handleDialogEvent(DialogEvent.Negative(requestId))
                }
            }
            neutralString?.also {
                setNeutralButton(neutralString) { _, _ ->
                    dialogProvider?.handleDialogEvent(DialogEvent.Neutral(requestId))
                }
            }
            items?.also {
                setItems(items) { _, i ->
                    dialogProvider?.handleDialogEvent(DialogEvent.SelectItem(requestId, i))
                }
            }
        }.create()
    }


    companion object {
        const val PARAM_REQUEST_ID = "param.dialog.request.id"
        const val PARAM_TITLE = "param.dialog.title"
        const val PARAM_MESSAGE = "param.dialog.message"
        const val PARAM_YES_STRING = "param.dialog.yes"
        const val PARAM_NO_STRING = "param.dialog.no"
        const val PARAM_NEUTRAL_STRING = "param.dialog.neutral"
        const val PARAM_ITEMS = "param.dialog.items"

        fun newInstance(requestId: Int, title: String?, message: String?, yesString: String?, noString: String?, neutralString: String?, list: Array<String>?) : AlertDialogFragment {
            return AlertDialogFragment().apply {
                arguments = Bundle().apply {
                    putInt(PARAM_REQUEST_ID, requestId)
                    title?.also { putString(PARAM_TITLE, it) }
                    message?.also { putString(PARAM_MESSAGE, it) }
                    yesString?.also { putString(PARAM_YES_STRING, it) }
                    noString?.also { putString(PARAM_NO_STRING, it) }
                    neutralString?.also { putString(PARAM_NEUTRAL_STRING, it) }
                    list?.also { putStringArray(PARAM_ITEMS, it) }
                }
            }
        }
    }

}