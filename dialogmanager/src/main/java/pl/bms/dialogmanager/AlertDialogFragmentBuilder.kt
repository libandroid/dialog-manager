@file:Suppress("unused")

package pl.bms.dialogmanager

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 18.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */


class AlertDialogFragmentBuilder(private val requestId: Int) {
    var title: String? = null
    var message: String? = null
    var positiveButton: String? = null
    var negativeButton: String? = null
    var neutralString: String? = null
    var items: Array<String>? = null

    fun create(): AlertDialogFragment {
        return AlertDialogFragment.newInstance(requestId, title, message, positiveButton, negativeButton, neutralString, items)
    }

}

