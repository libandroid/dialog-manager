package pl.bms.dialogmanagersample

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import pl.bms.dialogmanager.*
import java.util.concurrent.TimeUnit

class DialogsActivity : AppCompatActivity(), DialogFragmentProvider {

    private var disposable: Disposable? = null

    companion object {
        private const val REQUEST_CODE_DISMISS_PREVIOUS = 0
        private const val REQUEST_CODE_WAIT_ON_QUEUE = 1
        private const val REQUEST_CODE_SHOW_IF_NO_OTHERS = 2
        private const val REQUEST_CODE_SHOW_ON_TOP = 3
        private const val TAG = "DialogsActivity"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupView()
        initActions()

    }

    private fun setupView() {
        dialogsActivityNumberPicker.minValue = 1
        dialogsActivityNumberPicker.maxValue = 10
    }

    private fun initActions() {
        dialogsActivityDismissPrevious.setOnClickListener { startDialogs(DialogManager.ShowOption.DISMISS_PREVIOUS) }
        dialogsActivityWaitInQueue.setOnClickListener { startDialogs(DialogManager.ShowOption.WAIT_ON_QUEUE) }
        dialogsActivityShowIfNoOthers.setOnClickListener { startDialogs(DialogManager.ShowOption.SHOW_IF_NO_OTHERS) }
        dialogsActivityShowOnTop.setOnClickListener { startDialogs(DialogManager.ShowOption.SHOW_ON_TOP) }
    }

    override fun handleDialogEvent(dialogEvent: DialogEvent) {
        when (dialogEvent) {
            is DialogEvent.Positive -> {
                Log.d(TAG, "Positive action. Request id = ${dialogEvent.requestId}")
            }
            is DialogEvent.Negative -> {
                Log.d(TAG, "Negative action. Request id = ${dialogEvent.requestId}")
            }
            is DialogEvent.Neutral -> {
                Log.d(TAG, "Neutral action. Request id = ${dialogEvent.requestId}")
            }
            is DialogEvent.Cancel -> {
                Log.d(TAG, "Cancel action. Request id = ${dialogEvent.requestId}")
            }
            is DialogEvent.Destroy -> {
                Log.d(TAG, "Destroy action. Request id = ${dialogEvent.requestId}")
            }
        }
    }

    private fun startDialogs(option: DialogManager.ShowOption) {
        disposable?.dispose()
        disposable = Observable
                .intervalRange(1, dialogsActivityNumberPicker.value.toLong(), 0, 1, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    Log.d(TAG, "showDialog ${option.name}, requestCode: ${createRequestCode(option)}, number: $it")
                    showDialog(alertDialog(createRequestCode(option)) {
                        title = "Dialog option: ${option.name}"
                        message = "number $it"
                        positiveButton = "Positive"
                        negativeButton = "Negative"
                        neutralString = "Neutral"
                    }, option)
                }
    }

    private fun createRequestCode(option: DialogManager.ShowOption) : Int {
        return when (option) {
            DialogManager.ShowOption.DISMISS_PREVIOUS -> REQUEST_CODE_DISMISS_PREVIOUS
            DialogManager.ShowOption.WAIT_ON_QUEUE -> REQUEST_CODE_WAIT_ON_QUEUE
            DialogManager.ShowOption.SHOW_IF_NO_OTHERS -> REQUEST_CODE_SHOW_IF_NO_OTHERS
            DialogManager.ShowOption.SHOW_ON_TOP -> REQUEST_CODE_SHOW_ON_TOP
        }
    }

    override fun onResume() {
        super.onResume()
        DialogManager.registerActivity(this)
    }

    override fun onPause() {
        DialogManager.unregisterActivity(this)
        disposable?.dispose()
        disposable = null
        super.onPause()
    }

}
